CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

A simple module to enable the Drupal Commerce system to accept childcare
voucher payments.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/mjkovacevich/2508187

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2508187


REQUIREMENTS
------------

This module requires the following modules:

 * Drupal Commerce (https://drupal.org/project/commerce)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
-------------

 * On enabling the module a Childcare voucher payment method will be available
   under Store » Configuration » Payment methods.

 * After the Childcare voucher payment method is enabled it will appear in the
   payment method select list when adding a payment transaction to an order.


FAQ
---

Q: Why is the Commerce Childcare Voucher module not appearing in the list of
   modules?

A: Ensure that the module was installed properly. If it was installed try
   installing it again.


Q: Why do I not have a Childcare voucher option in the list of payment methods?

A: Check that the Childcare voucher payment method has been enabled.


Q: The date of the payment transaction gets overridden when I save confirm
   the voucher payment. Is this normal?

A: This is as designed. It makes reporting and running entity field queries
   easier and simplifies the reconciliation of bank accounts to transaction
   data. The original payment transaction dates can always be queried from
   the revisions.


MAINTAINERS
-----------

Current maintainers:

* Mike Kovacevich (Canalside) - http://drupal.org/user/54136

This project has been sponsored by:

* Canalside Software
  Specialists in Drupal powered solutions offering consulting, project
  management, installation, development and hosting services.
